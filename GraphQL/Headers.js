import { createHttpLink } from '@apollo/client';
import { setContext } from '@apollo/client/link/context';

const isBrowser = typeof window !== 'undefined';
console.log(isBrowser)
console.log(isBrowser ? window.location.origin : 'https://store.dev.180smoke.ca')

const httpLink = createHttpLink({
    uri: (isBrowser ? window.location.origin : 'https://store.dev.180smoke.ca') + `/graphql`,
});

const authLink = setContext((_, { headers }) => {
    const token = localStorage.getItem('token');
    return {
        headers: {
            ...headers,
            // authorization: token ? `Bearer ${token}` : `Basic ${process.env.API_AUTH}`,
            // authorization: `Basic ZGV2OnVWbjRATHVyZmRoV0doO0tzVHFnOHpWeFhlZHhieA==`,
            // "sec-fetch-mode": 'cors',
            // "Content-Type": "application/json"
        }
    }
});

const headers = authLink.concat(httpLink);

export default headers;
