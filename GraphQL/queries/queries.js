import { gql } from '@apollo/client';


const GET_BOOKS = gql`
query urlResolver($urlKey: String!) {
    urlResolver(url: $urlKey) {
        id
        type
        redirectCode
        relative_url
    }
}
`;

// const GET_PROVID = gql`
// ageVerificationLocations {
//     age
//     lifetime
//     name
//     region_id
// }
// `




export { GET_BOOKS };