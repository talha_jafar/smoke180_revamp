const { createProxyMiddleware } = require('http-proxy-middleware');
const dev = 'development' !== 'production';


// const getProxyConfig = path => {
//     console.log(path)
//   return {
//     target: 'https://store.dev.180smoke.ca' + path,
//     pathRewrite: { ['^' + path]: '/' },
//     secure: !dev,
//     changeOrigin: true,
//     onProxyRes: function(proxyRes, req, res) {
//       proxyRes.headers['Access-Control-Allow-Origin'] = null;
//     }
//   };
// };

// const devProxy = {
//   '/graphql': getProxyConfig('/graphql'),
// };

// module.exports = server => {
//   Object.keys(devProxy).forEach(function(context) {
//     server.use(proxyMiddleware(context, devProxy[context]));
//   });
// };
module.exports = function(app) {
    app.use(createProxyMiddleware('/graphql', { target: "https://store.dev.180smoke.ca/graphql" }));
};