// const express = require( 'express' );
// const next = require( 'next' );
// const proxy = require('./server_proxy');
// const dev = 'development' !== 'production';
// console.log('dev :',dev);
// const app = next({ dev });


// app.prepare()
//     .then(() => {
//         const server = express();

//         proxy(server)

//     }).catch(error=> {
//         console.error(error.stack);
//         process.exit(1);
//     })

const express = require('express');
const next = require('next');
const { createProxyMiddleware } = require("http-proxy-middleware")
const port = 3000;
const dev = "development" !== 'production';
console.log('dev :', dev);
const app = next({ dev });
const handle = app.getRequestHandler();

app.prepare()
    .then(() => {
        const server = express();
        
        server.use(createProxyMiddleware('/graphql', { target: "https://store.dev.180smoke.ca/graphql" , changeOrigin:true }));

        server.listen(port, err => {
            if (err) {
                throw err
            } else {
                console.log(`server started ${port}`);
            }
        })
    }).catch(error => {
        console.error(error.stack);
        process.exit(1);
    })