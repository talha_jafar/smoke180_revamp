import '../styles/globals.css'
import '../styles/sass/home.scss'
import Head from 'next/head'
const { default: Footer } = require("./structure/Footer")
const { default: Nav } = require("./structure/Nav")


const Layout = (props) => {
    return (
        <div>
            <Head>
                <title>180 Smoke</title>
                <link rel="icon" href="/favicon.ico" />
            </Head>
            <Nav />
            <div style={{ display: 'flex', flexDirection: 'column', alignItems: 'center' }}>
                {props.children}
            </div>
            <Footer />
        </div>
    )
}

export default Layout;