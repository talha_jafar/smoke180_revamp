import { useEffect, useState } from 'react';
import '../../styles/styles.css'
import AliceCarousel from 'react-alice-carousel';
import "react-alice-carousel/lib/alice-carousel.css";


let images = [
    <img src="https://store.dev.180smoke.ca/media/aw_rbslider/slides/ccd_pacha_desktop_en.jpg"/>,
    <img src="https://store.dev.180smoke.ca/media/aw_rbslider/slides/Banner_Desktop_Buy-2-Get-1-Eliquid.jpg"/>,
    <img src="https://store.dev.180smoke.ca/media/aw_rbslider/slides/ccd_pacha_desktop_en.jpg"/>,
    <img src="https://store.dev.180smoke.ca/media/aw_rbslider/slides/Banner_Desktop_Buy-2-Get-1-Eliquid.jpg"/>
    
];

const responsive = {
    0: { items: 1 },
    1024: { items: 1 },
}


const AliceCarousell = () => {
    const [load, setLoad] = useState(true);

    return (
        <div className="w-100">
            {load ? <AliceCarousel
                items={images}
                responsive={responsive}
                autoPlayInterval={2000}
                autoPlayDirection="rtl"
                autoPlay={true}
                fadeOutAnimation={true}
                mouseTrackingEnabled={true}
                disableAutoPlayOnAction={true}
            /> : null}
        </div>
    )
}

export default AliceCarousell;