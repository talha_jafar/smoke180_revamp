import { useEffect, useState } from 'react';


import AliceCarousel from 'react-alice-carousel';
import "react-alice-carousel/lib/alice-carousel.css";
import Card from 'react-bootstrap/Card';
import axios from 'axios';

let products = [];

const responsive = {
    0: { items: 1 },
    768: { items: 4 },
}

const addItems = () => {
    for (var i = 0; i < 5; i++) {
        products.push(<Card style={{ width: '18rem' }}>
            <Card.Img variant="top" src="https://picsum.photos/id/1001/5616/3744" />
            <Card.Body>
                <Card.Title>Card Title</Card.Title>
                <Card.Text>
                    Some quick example text to build on the card title and make up the bulk of
                    the card's content.
          </Card.Text>
                {/* <Button variant="primary">Go somewhere</Button> */}
            </Card.Body>
        </Card>);
    }
}

const FeaturedProducts = () => {
    addItems();
    return (
        <>
            <div className="featured-div">
                <h1>Featured Products</h1>
                <hr/>
                {/* <div style={{ marginBottom: 15 }}>
                    <h1>_________</h1>
                </div> */}
                <AliceCarousel
                    items={products}
                    responsive={responsive}
                    autoPlayInterval={2000}
                    autoPlayDirection="rtl"
                    autoPlay={true}
                    fadeOutAnimation={true}
                    mouseTrackingEnabled={true}
                    disableAutoPlayOnAction={true}
                />
            </div>
        </>
    )
}

export default FeaturedProducts;