import { Facebook, Instagram, Linkedin, Twitter, Yelp, Youtube } from './Icons';
import '../../styles/styles.css';
const links = {
  instagram: 'https://www.instagram.com/180smoke/',
  facebook: 'https://www.facebook.com/180Smoke',
  youtube: 'https://www.youtube.com/channel/UCi8p8cIFzikgMA7BD1Q2TIA/',
  yelp: 'https://www.yelp.ca/biz/180-smoke-vape-store-toronto-2',
  twitter: 'https://twitter.com/180Smoke',
  linkedin: 'https://www.linkedin.com/company/180-smoke'
};

const Footer = () => {
  return (
    <div style={{ display: 'flex', flexDirection: 'column', alignItems: 'center' }}>
      <h2 >
        Join the Conversation
        <span className={'hide-mobile'}> on Social</span>
      </h2>
      <ul className="d-flex flex-wrap justify-content-center social-section__list ">
        {links.instagram && (
          <li>
            <a href={links.instagram} target="_blank" rel="noopener noreferrer">
              <Instagram width={70} />
            </a>
          </li>
        )}
        {links.facebook && (
          <li>
            <a href={links.facebook} target="_blank" rel="noopener noreferrer">
              <Facebook width={70} />
            </a>
          </li>
        )}
        {links.youtube && (
          <li>
            <a href={links.youtube} target="_blank" rel="noopener noreferrer">
              <Youtube width={70} />
            </a>
          </li>
        )}
        {links.yelp && (
          <li>
            <a href={links.yelp} target="_blank" rel="noopener noreferrer">
              <Yelp width={70} />
            </a>
          </li>
        )}
        {links.twitter && (
          <li>
            <a href={links.twitter} target="_blank" rel="noopener noreferrer">
              <Twitter width={70} />
            </a>
          </li>
        )}
        {links.linkedin && (
          <li>
            <a href={links.linkedin} target="_blank" rel="noopener noreferrer">
              <Linkedin width={70} />
            </a>
          </li>
        )}
      </ul>
      <div className='footer-links'>
        <ul className='social-links-container'>
          <li>
            <h4><a href="https://www.180smoke.ca/about-us">180 Smoke Vape Store</a></h4>
            <ul>
              <li><a href="https://www.180smoke.ca/vape-shops-near-me">Store Locator</a></li>
              <li><a href="https://www.180smoke.ca/vape-shops-near-me">Privacy</a></li>
              <li><a href="https://www.180smoke.ca/vape-shops-near-me">Terms And Conditions</a></li>
            </ul>
          </li>
          <li>
            <h4><a href="https://www.180smoke.ca/about-us">Support</a></h4>
            <ul>
              <li><a href="https://www.180smoke.ca/vape-shops-near-me">(855) 994-6180</a></li>
              <li><a href="https://www.180smoke.ca/vape-shops-near-me">support@180smoke.com</a></li>
            </ul>
          </li>
          <li>
            <h4><a href="https://www.180smoke.ca/about-us">Orders</a></h4>
            <ul>
              <li><a href="https://www.180smoke.ca/vape-shops-near-me">Check Order Status</a></li>
              <li><a href="https://www.180smoke.ca/vape-shops-near-me">General Warranty & Returns</a></li>
              <li><a href="https://www.180smoke.ca/vape-shops-near-me">Vaporizer Warranty & Returns</a></li>
              <li><a href="https://www.180smoke.ca/vape-shops-near-me">Check Order Status</a></li>
              <li><a href="https://www.180smoke.ca/vape-shops-near-me">Shipping & Tracking</a></li>
              <li><a href="https://www.180smoke.ca/vape-shops-near-me">Where to Buy IQOS</a></li>
            </ul>
          </li>
        </ul>
      </div>
      <div className='footer-disclaimer'>
        <div className='footer-disclaimer-child'>
          <p className='font-semibold text-lg mb-4'>You must be at least the Age of Majority to buy and/or use this website. I.D. is required for delivery and pickup.</p>
          <p>You must be at least the Age of Majority (19+ in Ontario) to buy and/or use this website. Government-issued identification is required for delivery and pickup. Products containing nicotine on this website may be hazardous and addictive. All products sold by 180 Smoke Vape Store are intended for use by adult smokers only, and are not intended for pregnant or nursing women; children; people with, or at risk, for heart disease; high blood pressure; diabetes; asthma; or those who are sensitive to propylene glycol, nicotine, or vegetable glycerine. Information and statements regarding these products have not been evaluated by Health Canada or the FDA. Please be responsible with e-liquid and vape devices, and keep them stored out of reach of children and pets—they can be fatal to them, even in small quantities!</p>
        </div>
      </div>
      <div className='footer-copyright'>
        <div className='footer-copyright-child'>
          <p>© 2019 180 Smoke Vape Store. All Rights Reserved.</p>
        </div>
      </div>
    </div>
  );
}

export default Footer;